# La Tournée - Backend home assignment

## Description

This repostory is a backend home assignment for an open position at La Tournée. The assignment's subject is available at https://gist.github.com/ThomasLaTournee/672b528a807f444ae16bf7bb9b7754a2

Given the limited time (4 hours), I managed to:
- initialize the project (git, poetry and a set of dependencies)
- create the project's structure
- create the route
- implement a subset of the desirated behavior for the controller
- test the implemented subset

## How I worked

I started by initializing an empty git repository and decided the tools I would need:

- poetry for managing my dependencies
- ruff for formating and linting
- mypy for static checking
- pytest for testing
- pre-commit to run ensure the code quality before committing

Not everything is setup as properly as I wanted and with more time I would have:

- added test coverage verifying
- configured mypy to work nicely with pydantic
- included mypy and pytest in pre-commit hooks
- added a gitlab-ci.yml file to configure a nice pipeline

Once the project was initialized, I added my main dependencies (FastAPI, Pydantic and Uvicorn) and created the basic code structure:

- models
- fake db layer
- controller
- routing
- settings
- exceptions

Each time I tried to follow best practices, using FastAPI lastest recommandations (like dependencies injection).

Now that I had a working project, I started to implement the `dispatch` function of the controller. Since there are a lots of rules, I decided to take a TDD approache and iterate over my implementation:

- use supplier crates
- handle one product (first small, then large) and dispatch in the correct creates
- handle several products of the same size
- handle a mix of small and large products
- handle some edge cases (sku not found for example)

Given the limited time, I couldn't finish the implementation and both the Orangina and the La Tournée packing specificities are not handled. But I already wrote the test for Orangina bottles and it is expected to fail.

I finished my work by adding some documentation, including the Readme.

## With more time I would have

- added coverage
- tested the route to ensure status codes and payloads are corrects
- added example in the OpenAPI generated schema (in pydantic models and @router decorators)
- finished the implementation of special cases (Orangina + La Tournée packing)
- generated the documentation using Sphinx
- ensure all edge cases are handled
- added a nice pipeline to ensure code quality
- added more git hooks to avoid "fix lint/test/format" commits ;)

## Installation

This projects requires [Python >= 3.10](https://www.python.org/downloads/) and uses [Poetry](https://python-poetry.org/) to manage its dependencies.

Once Poetry is intalled, hit `poetry install --with dev` in the cloned git repository.

You can also install pre-commit hooks by running `poetry run pre-commit install`.

## Usage

To start the API, hit `poetry run python -m latournee` and visit `http://127.0.0.1:8000/docs`.