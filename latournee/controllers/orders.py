"""Contains the logic of Orders."""
from latournee.db.session import DBSession
from latournee.exceptions import SKUNotFoundException
from latournee.models import Dispatch, Order, PackingSize


class OrdersController:
    """A controller to manage orders."""

    def __init__(self, db_session: DBSession) -> None:
        """Initialize a new :class:`OrdersController` instance.

        :param db_session: the db session to use to retrieve data
        """
        self._db_session = db_session

    async def dispatch(self, orders: list[Order]) -> Dispatch:
        """Given a list of orders, compute the number of crate of each type needed.

        :param orders: a list of orders
        :raises SKUNotFoundException: raised if a product in the order doesn't exist
        :return: the number of crate of each type needed
        """
        slot20_remaining_slots = 0
        slot12_remaining_slots = 0
        result = Dispatch()

        for order in orders:
            try:
                product = self._db_session.store[order.sku]
            except KeyError as e:
                raise SKUNotFoundException(order.sku) from e

            # Use as many supplier crates as possible
            if product.preparation_in_crate:
                result.supplier += order.unit_count // product.packing
                remaining = order.unit_count % product.packing
            else:
                remaining = order.unit_count

            # Fill La Tournée crates with remaining
            packing_size = PackingSize.from_product(product)

            if packing_size == PackingSize.Small:
                slot20_remaining_slots = await self._pack_small_product(
                    remaining, result, slot20_remaining_slots
                )

            else:
                slot12_remaining_slots = await self._pack_large_product(
                    remaining, result, slot12_remaining_slots
                )

        # If we have more than 5 remaining slots in a slot12 crate, replace it with a slot6 one
        if slot12_remaining_slots >= 6:
            result.slot12 -= 1
            result.slot6 += 1

        return result

    async def _pack_small_product(
        self, count: int, ongoing_result: Dispatch, free_slots: int
    ) -> int:
        """Pack a small product

        :param count: how many items of the product must be packed
        :param ongoing_result: the current used crates
        :param free_slots: how many slot20 slots are available in the partially filled slot20 (if any)
        :return: how many free slots are available in the partially filled slot20 crate
        """
        ongoing_result.slot20 += count // 20
        count = count % 20
        if count > 0:
            # If we already have a partially filled slot20 crate,
            # fill it first before adding a new crate
            if free_slots > 0:
                delta = min(count, free_slots)
                count -= delta
                free_slots -= delta

            # if there are still remaining products, add a new slot20 create
            if count > 0:
                ongoing_result.slot20 += 1
                free_slots = 20 - count

        return free_slots

    async def _pack_large_product(
        self, count: int, ongoing_result: Dispatch, free_slots: int
    ) -> int:
        """Pack a large product

        :param count: how many items of the product must be packed
        :param ongoing_result: the current used crates
        :param free_slots: how many slot12 slots are available in the partially filled slot12 (if any)
        :return: how many free slots are available in the partially filled slot12 crate
        """
        ongoing_result.slot12 += count // 12
        count = count % 12
        if count > 0:
            # If we already have a partially filled slot12 crate,
            # fill it first before adding a new crate
            if free_slots > 0:
                delta = min(count, free_slots)
                count -= delta
                free_slots -= delta

            # if there are still remaining products, add a new slot20 create
            if count > 0:
                ongoing_result.slot12 += 1
                free_slots = 12 - count

        return free_slots
