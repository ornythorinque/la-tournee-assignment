from pathlib import Path

import pytest

from latournee.controllers.orders import OrdersController
from latournee.db.session import DBSession
from latournee.exceptions import (
    SKUNotFoundException,
    UnknownProductPackingSizeException,
)
from latournee.models import Dispatch, Order


class TestOrdersController:
    @pytest.fixture(scope="class")
    def controller(self) -> OrdersController:
        """A working controller with some data loaded.

        :return: the controller instance
        """
        return OrdersController(DBSession(Path(__file__).parent / "data.json"))

    async def test_dispatch_single_large_product_with_supplier_crate(
        self, controller: OrdersController
    ):
        """Test orders with a single large product type that allows supplier crates."""
        # Exactly one supplier crate
        disp = await controller.dispatch(
            [
                Order(
                    ID="1",
                    OrderID="aaa",
                    SKU="pajo-jus-multifruits-bio-75",
                    UnitCount=20,
                )
            ]
        )
        assert Dispatch(supplier=1) == disp

        # One supplier crate + less than 6 slots
        disp = await controller.dispatch(
            [
                Order(
                    ID="1",
                    OrderID="aaa",
                    SKU="pajo-jus-multifruits-bio-75",
                    UnitCount=22,
                )
            ]
        )
        assert Dispatch(supplier=1, slot6=1) == disp

        # One supplier crate + more than 6 slots but less than 12
        disp = await controller.dispatch(
            [
                Order(
                    ID="1",
                    OrderID="aaa",
                    SKU="pajo-jus-multifruits-bio-75",
                    UnitCount=28,
                )
            ]
        )
        assert Dispatch(supplier=1, slot12=1) == disp

        # A more complex scenario with a combination of supplier, 6 and 12 crates
        disp = await controller.dispatch(
            [
                Order(
                    ID="1",
                    OrderID="aaa",
                    SKU="pajo-jus-multifruits-bio-75",
                    UnitCount=36,
                )
            ]
        )
        assert Dispatch(supplier=1, slot6=1, slot12=1) == disp

    async def test_dispatch_single_small_product_with_supplier_crate(
        self, controller: OrdersController
    ):
        """Test orders with a single small product type that allows supplier crates."""

        # Exactly one supplier crate
        disp = await controller.dispatch(
            [Order(ID="1", OrderID="aaa", SKU="coca-cola-33", UnitCount=24)]
        )
        assert Dispatch(supplier=1) == disp

        # Supplier crates + 20 slots one
        disp = await controller.dispatch(
            [Order(ID="1", OrderID="aaa", SKU="coca-cola-33", UnitCount=50)]
        )
        assert Dispatch(supplier=2, slot20=1) == disp

    async def test_dispatch_single_large_product_no_supplier_crate(
        self, controller: OrdersController
    ):
        # Matches the packing
        disp = await controller.dispatch(
            [
                Order(
                    ID="1",
                    OrderID="aaa",
                    SKU="la-tournee-muesli-croustillant-chocolat-430",
                    UnitCount=34,
                )
            ]
        )
        assert Dispatch(slot12=3) == disp

    async def test_dispatch_single_small_product_no_supplier_crate(
        self, controller: OrdersController
    ):
        """Test orders with a single small product type that forbid supplier crates."""

        # Matches the packing
        disp = await controller.dispatch(
            [
                Order(
                    ID="1",
                    OrderID="aaa",
                    SKU="la-tournee-sables-parmesan-110",
                    UnitCount=36,
                )
            ]
        )
        assert Dispatch(slot20=2) == disp

    async def test_dispatch_multiple_large_products(self, controller: OrdersController):
        """Test orders with a multiple large product types."""

        disp = await controller.dispatch(
            [
                Order(
                    ID="1",
                    OrderID="aaa",
                    SKU="pajo-jus-multifruits-bio-75",
                    UnitCount=45,
                ),
                Order(
                    ID="2",
                    OrderID="aaa",
                    SKU="pajo-jus-pomme-gingembre-bio-75",
                    UnitCount=5,
                ),
            ]
        )
        assert Dispatch(supplier=2, slot12=1) == disp

        disp = await controller.dispatch(
            [
                Order(
                    ID="1",
                    OrderID="aaa",
                    SKU="pajo-jus-multifruits-bio-75",
                    UnitCount=45,
                ),
                Order(
                    ID="2",
                    OrderID="aaa",
                    SKU="pajo-jus-pomme-gingembre-bio-75",
                    UnitCount=1,
                ),
            ]
        )
        assert Dispatch(supplier=2, slot6=1) == disp

    async def test_dispatch_multiple_small_products(self, controller: OrdersController):
        """Test orders with a multiple small product types."""

        disp = await controller.dispatch(
            [
                Order(ID="1", OrderID="aaa", SKU="coca-cola-33", UnitCount=50),
                Order(
                    ID="2",
                    OrderID="aaa",
                    SKU="la-tournee-sables-parmesan-110",
                    UnitCount=10,
                ),
                Order(
                    ID="3",
                    OrderID="aaa",
                    SKU="la-tournee-pois-chiches-345",
                    UnitCount=8,
                ),
            ]
        )
        assert Dispatch(supplier=2, slot20=1) == disp

        disp = await controller.dispatch(
            [
                Order(ID="1", OrderID="aaa", SKU="coca-cola-33", UnitCount=50),
                Order(
                    ID="2",
                    OrderID="aaa",
                    SKU="la-tournee-sables-parmesan-110",
                    UnitCount=10,
                ),
                Order(
                    ID="3",
                    OrderID="aaa",
                    SKU="la-tournee-pois-chiches-345",
                    UnitCount=10,
                ),
            ]
        )
        assert Dispatch(supplier=2, slot20=2) == disp

    async def test_dispatch_multiple_small_and_large_products(
        self, controller: OrdersController
    ):
        """Test orders with a multiple large and small product types."""

        disp = await controller.dispatch(
            [
                Order(ID="1", OrderID="aaa", SKU="coca-cola-33", UnitCount=50),
                Order(
                    ID="2",
                    OrderID="aaa",
                    SKU="la-tournee-sables-parmesan-110",
                    UnitCount=10,
                ),
                Order(
                    ID="3",
                    OrderID="aaa",
                    SKU="la-tournee-pois-chiches-345",
                    UnitCount=6,
                ),
                Order(
                    ID="4",
                    OrderID="aaa",
                    SKU="pajo-jus-multifruits-bio-75",
                    UnitCount=45,
                ),
            ]
        )
        assert Dispatch(supplier=4, slot20=1, slot6=1) == disp

    async def test_dispatch_orangina_bottles_special_case(
        self, controller: OrdersController
    ):
        """Test orders containing the special case 'Orangina bottles'."""

        # Only orangina bottles
        disp = await controller.dispatch(
            [
                Order(ID="1", OrderID="aaa", SKU="orangina-25", UnitCount=40),
            ]
        )
        assert Dispatch(slot12=2) == disp

        # Mix with other products
        disp = await controller.dispatch(
            [
                Order(ID="1", OrderID="aaa", SKU="orangina-25", UnitCount=40),
                Order(
                    ID="2",
                    OrderID="aaa",
                    SKU="pajo-jus-multifruits-bio-75",
                    UnitCount=6,
                ),
            ]
        )
        assert Dispatch(slot12=2, slot6=1) == disp

    async def test_dispatch_latournee_containers_special_case(
        self, controller: OrdersController
    ):
        """Test orders containing the special case 'La Tournée containers'."""

        pass

    async def test_dispatch_unknown_sku(self, controller: OrdersController):
        """Test an order with a non-existing SKU."""

        with pytest.raises(SKUNotFoundException):
            await controller.dispatch(
                [Order(ID="1", OrderID="aaa", SKU="does-not-exist", UnitCount=50)]
            )

    async def test_packing_size_error(self) -> None:
        """Test an order where we can't compute the packing size (large or small)."""

        controller = OrdersController(
            DBSession(Path(__file__).parent / "packing_error.json")
        )
        with pytest.raises(UnknownProductPackingSizeException):
            await controller.dispatch(
                [
                    Order(
                        ID="1",
                        OrderID="aaa",
                        SKU="pajo-jus-multifruits-bio-75",
                        UnitCount=50,
                    )
                ]
            )
