"""A collection of tests related to the :class:`DBSession` class."""
import pathlib

from latournee.db.session import DBSession
from latournee.models import Product


async def test_data_loading() -> None:
    """Load a custom data file and verify content."""
    session = DBSession(pathlib.Path(__file__).parent / "data.json")
    assert session.store == {
        "la-tournee-melange-fruits-secs-230": Product(
            sku="la-tournee-melange-fruits-secs-230",
            brand="La Tournée",
            packing=43,
            deposit=0.2,
            preparation_in_crate=False,
        ),
        "la-tournee-yaourt-framboise-350": Product(
            sku="la-tournee-yaourt-framboise-350",
            brand="La Tournée",
            packing=12,
            deposit=0.2,
            preparation_in_crate=False,
        ),
    }
