"""Contains classes that fake a DB interaction layer."""
import json
from os import fspath
from pathlib import Path

from latournee.models import Product


class DBSession:
    """
    A fake database session loading data from a JSON file.

    Once initialized, the data is stored in the `store` attribute as a mapping sku to :class:`Product`.
    """

    @staticmethod
    def load_data(filename: str) -> list[dict]:
        """Load data from a JSON file.

        :param filename: path to the JSON file
        :return: a list of products, each one being a key-value mapping
        """
        with open(filename, "r", encoding="utf-8") as filed:
            return json.load(filed)

    def __init__(self, data_path: Path) -> None:
        """Initialise a new DBSession instance.

        :param filename: Path to the data file.
        """
        self.store: dict[str, Product] = {}
        for raw in DBSession.load_data(fspath(data_path)):
            product = Product(**raw)
            self.store[product.sku] = product
