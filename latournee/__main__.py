"""Main entrypoint of the application"""
import uvicorn
from fastapi import FastAPI

from latournee.dependencies import get_settings
from latournee.routing.orders import router as orders_router


def create_app() -> FastAPI:
    """Create the main application"""
    app = FastAPI(title="La Tournée API")
    app.include_router(orders_router)

    return app


if __name__ == "__main__":
    settings = get_settings()
    uvicorn.run(create_app(), host=settings.web_host, port=settings.web_port)
