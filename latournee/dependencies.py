"""Contains the """
from functools import lru_cache
from typing import Annotated

from fastapi import Depends

from latournee.controllers.orders import OrdersController
from latournee.db.session import DBSession
from latournee.settings import Settings


@lru_cache
def get_settings() -> Settings:
    """Create the application settings.

    :return: a :class:`Settings` instance
    """
    return Settings()


async def get_db_session(
    settings: Annotated[Settings, Depends(get_settings)]
) -> DBSession:
    """Get a db session.

    :param settings: the app settings
    :return: a :class:`DBSession` instance
    """
    return DBSession(settings.db_path)


async def get_orders_controller(
    db_session: Annotated[DBSession, Depends(get_db_session)]
) -> OrdersController:
    """Get an orders controller.

    :param db_session: the db session
    :return: an :class:`OrdersController` instance
    """
    return OrdersController(db_session)
