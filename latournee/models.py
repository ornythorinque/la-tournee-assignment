"""A collection of models used in the application."""
from __future__ import annotations

import enum
from typing import Annotated

from pydantic import BaseModel, Field
from pydantic.types import NonNegativeFloat, NonNegativeInt

from latournee.exceptions import UnknownProductPackingSizeException


class Product(BaseModel):
    """A product as stored in the database."""

    sku: str
    brand: str
    packing: NonNegativeInt
    deposit: NonNegativeFloat
    preparation_in_crate: bool


class Order(BaseModel):
    """An 'order part' of a user's order."""

    id: Annotated[str, Field(validation_alias="ID")]
    order_id: Annotated[str, Field(validation_alias="OrderID")]
    sku: Annotated[str, Field(validation_alias="SKU")]
    unit_count: Annotated[int, Field(gt=0, validation_alias="UnitCount")]


class Dispatch(BaseModel):
    """A description of crates to be used for a given order."""

    supplier: Annotated[int, Field(0, ge=0, serialization_alias="Supplier")]
    slot6: Annotated[int, Field(0, ge=0, serialization_alias="Slot6")]
    slot12: Annotated[int, Field(0, ge=0, serialization_alias="Slot12")]
    slot20: Annotated[int, Field(0, ge=0, serialization_alias="Slot20")]


class PackingSize(enum.Enum):
    """An enum to distinguish packing sizes."""

    Small = 0
    Large = 1

    @staticmethod
    def from_product(product: Product) -> PackingSize:  # noqa: F821
        if abs(product.deposit - 0.2) <= 0.01:
            return PackingSize.Small
        elif abs(product.deposit - 0.4) <= 0.01:
            return PackingSize.Large
        raise UnknownProductPackingSizeException(product.deposit)
