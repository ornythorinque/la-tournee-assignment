"""A collection of exceptions used in the application."""
from typing import Any

from fastapi import HTTPException


class NotFoundException(HTTPException):
    """Base exception for Not Found exceptions."""

    def __init__(self, detail: Any = None) -> None:
        super().__init__(404, detail)


class SKUNotFoundException(NotFoundException):
    """An exception thrown when a product's SKU is not found in the database."""

    def __init__(self, sku: str) -> None:
        super().__init__(f"sku not found: {sku}")


class ServerException(HTTPException):
    """Base exception for server-side exceptions."""

    def __init__(self, detail: Any = None) -> None:
        super().__init__(500, detail)


class UnknownProductPackingSizeException(ServerException):
    """An exception thrown when a product's packing type can't be computed."""

    def __init__(self, deposit: float) -> None:
        super().__init__(f"Can't match deposit ({deposit}) with packing size")
