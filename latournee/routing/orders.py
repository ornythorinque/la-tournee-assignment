"""This module contains all the routes for the 'orders' router."""
from typing import Annotated

from fastapi import APIRouter, Depends

from latournee.controllers.orders import OrdersController
from latournee.dependencies import get_orders_controller
from latournee.models import Dispatch, Order

router = APIRouter(prefix="/orders", tags=["Orders"])


@router.post("/dispatch", response_model=Dispatch)
async def dispatch(
    orders: list[Order],
    ctrl: Annotated[OrdersController, Depends(get_orders_controller)],
) -> Dispatch:
    """Given an order, compute the products dispatch in the different crates."""
    return await ctrl.dispatch(orders)
