"""Contais the application settings."""
from pathlib import Path

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    """The application settings."""

    db_path: Path = Path(__file__).parent / "db" / "data.json"
    web_port: int = 8000
    web_host: str = "127.0.0.1"

    model_config = SettingsConfigDict(env_prefix="LATOURNEE_")
